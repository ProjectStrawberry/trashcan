package com.calderaminecraft.trashcan;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private static Main plugin;
    static InventoryManager inventoryManager = new InventoryManager();

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(inventoryManager, this);
        this.getCommand("dispose").setExecutor(new CommandDispose());

        getLogger().info("TrashCan loaded.");
    }
}
