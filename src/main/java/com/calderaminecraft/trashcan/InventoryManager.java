package com.calderaminecraft.trashcan;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.UUID;

public class InventoryManager implements Listener {
    private HashMap<UUID, Inventory> inventories = new HashMap<>();

    void openInventory(Player player) {
        if (!inventories.containsKey(player.getUniqueId())) {
            inventories.put(player.getUniqueId(), Bukkit.createInventory(null, 36, ChatColor.DARK_GRAY + ChatColor.BOLD.toString() + "Trash Can"));
        }

        player.openInventory(inventories.get(player.getUniqueId()));
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        HumanEntity player = event.getPlayer();
        Inventory inv = event.getInventory();
        if (!(player instanceof Player)) return;

        if (inventories.containsKey(player.getUniqueId())) {
            Inventory playerInv = inventories.get(player.getUniqueId());
            if (inv.equals(playerInv)) {
                playerInv.clear();
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        inventories.remove(player.getUniqueId());
    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        Player player = event.getPlayer();
        inventories.remove(player.getUniqueId());
    }
}
